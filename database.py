import sqlalchemy
import datetime
import os.path

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy	import (
	Column,
	Integer,
	String,
	Float,
	DateTime,
	Time,
	Boolean,
	create_engine,
	ForeignKey
)

from sqlalchemy.orm import relationship, backref, sessionmaker

# engine = create_engine('sqlite:///foo.db')
DATABASE = 'sqlite:///db.sqlite3'
DEBUG = True
Base = declarative_base()

# class UserPreferences(Base):
# 	__tablename__ = 'user_preferences'

# 	id = Column(Integer, primary_key=True, autoincrement=True)
# 	type_of_cacao = Column(String(15))
# 	desired_moisture_content = Column(Float)
# 	user_batch_name = Column(String(20))

# class BatchData(Base):
# 	__tablename__ = 'batch_data'

# 	batch_id = Column(String(50), primary_key=True)
# 	batch_datetime = Column(DateTime, default=datetime.datetime.now())
# 	user_pref = Column(Integer, ForeignKey('user_preferences.id'))

class UserPreferences(Base):
	__tablename__ = 'user_preference'

	id = Column(Integer, primary_key=True, autoincrement=True)
	user_batch_name = Column(String(20))
	#Commented for new ERD
	# same_batch_id = Column(Integer, ForeignKey('batch_data.id'))
	# same_batch = relationship('BatchData', backref=backref('user_preferences', uselist=True, cascade='delete, all'))
	desired_moisture = Column(Float)
	type_of_cacao = Column(String(15))
	date_created = Column(DateTime, default=datetime.datetime.now())

class BatchData(Base):
	__tablename__ = 'batch_data'

	id = Column(Integer, primary_key=True, autoincrement=True)
	batch_datetime = Column(DateTime, default=datetime.datetime.now())
	batch_type_of_cacao = Column(String(15))
	batch_desired_moisture_content = Column(Float)
	batch_name = Column(String(20))
	# user_pref_id = Column(Integer, ForeignKey('user_preferences.id'))
	# user_pref = relationship('UserPreferences', back_populates='batch_data')

class EnvVars(Base):
	__tablename__ = 'environment_vars'

	id = Column(Integer, primary_key=True, autoincrement=True)
	# batch_info = Column(String(50), ForeignKey('batch_data.id'))
	batch_info = Column(Integer, ForeignKey('batch_data.id'))
	initial_data = Column(Boolean, default = False)
	temperature = Column(Float)
	#Commented out for Simulation Purposes
	# humidity = Column(Float)
	dry_weight = Column(Float)
	read_time = Column(Time, default=datetime.datetime.now().time())
	graphed = Column(Boolean, default=False)

class CompsData(Base):
	__tablename__ = 'comps_data'

	id = Column(Integer, primary_key=True, autoincrement=True)
	read_time_c = Column(Time, ForeignKey('environment_vars.read_time'))
	moisture_content = Column(Float)
	approx_drying_time = Column(DateTime)
	drying_rate = Column(Float)
	read_time = Column(Time, default=datetime.datetime.now().time())

engine = create_engine(DATABASE, echo=DEBUG)
session_factory = sessionmaker(bind=engine)
session = session_factory()

if not os.path.exists('db.sqlite3'):
	Base.metadata.create_all(engine)