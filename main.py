import matplotlib
matplotlib.use('module://kivy.garden.matplotlib.backend_kivy')
import kivy
import sqlalchemy
import datetime
import os.path
import sqlite3
# import RPi.GPIO as GPIO

from database import *

from kivy.config import Config
Config.set('graphics', 'resizable', False)
Config.set('graphics', 'width', '630')
Config.set('graphics', 'height', '300')
from kivy.core.window import Window
Window.clearcolor = (1,1,1,1)

from functools import wraps
from kivy.app import App
from kivy.base import runTouchApp
from kivy.clock import Clock
from kivy.properties import  ObjectProperty, BooleanProperty, StringProperty, ListProperty, AliasProperty, NumericProperty
from kivy.uix.behaviors import FocusBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition
from kivy.uix.label import Label
from kivy.uix.widget import Widget
from kivy.uix.dropdown import DropDown
from kivy.uix.popup import Popup
from kivy.uix.spinner import Spinner
from kivy.uix.textinput import TextInput
from kivy.uix.actionbar import ActionItem
from kivy.uix.recyclegridlayout import RecycleGridLayout
from kivy.uix.recycleview import RecycleView
from kivy.uix.recycleview.layout import LayoutSelectionBehavior
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.recycleboxlayout import RecycleBoxLayout
from kivy.uix.gridlayout import GridLayout

import matplotlib.pyplot as plt

from random import randint

from kivy.clock import Clock

from sqlalchemy import exists

# from kivy.utils.get_color_from_hex import hex

fig, (ax,ax2) = plt.subplots(2,1, sharex=True)

xarr = []
yarr = []
zarr = []

timer = 0

checkid = 0

canvas = fig.canvas

counter = 0
wetw = 0
dryw = 0
moistContent = 0

class Display(BoxLayout):
    pass

class ScrOne(Screen):

    def warning_pop(self):
        app = App.get_running_app()
        layout = BoxLayout(orientation='vertical')
        layout.add_widget(Label(text='There is no preset selected'))
        closeButton = Button(text='Close',
            background_color=app.NEGATIVE_COLOR,
            background_normal=app.NEUTRAL
            )

        layout.add_widget(closeButton)

        popup = Popup(title='Warning!',
            content=layout,
            size_hint= [0.5, 0.7],
            auto_dismiss=False)

        popup.open()
        closeButton.bind(on_press=popup.dismiss)

    def check_val1(self):
        '''for [Apply Button]'''
        app = App.get_running_app()

        if self.ids.search_preset.text != 'Press to search for saved presets...':
            print '*-- ApplyButton pressed --*'

            sel = self.ids.search_preset.text
            app.USER_SELECTION = sel

            # ScrThree().load_details()
            self.manager.current = 'load_cacao'

            self.manager.get_screen('load_cacao').load_details()
            # .batch_name = app.USER_SELECTION
            # ScrThree().load_details()

        else:
            print 'no'
            ScrOne().warning_pop()

    def check_val2(self):
        '''for [Delete Button]'''
        if self.ids.search_preset.text != 'Press to search for saved presets...':
            print '*-- delete button pressed --*'

            app = App.get_running_app()

            app.USER_SELECTION = self.ids.search_preset.text

            layout = BoxLayout(orientation='vertical')
            layout2 = BoxLayout(orientation='horizontal')
            layout.add_widget(Label(text='Are you sure you want to delete?'))

            deleteButton = Button(text='Yes, delete it',
                background_color=app.POSITIVE_COLOR,
                background_normal=app.NEUTRAL
                )
            cancelButton = Button(text='Cancel',
                background_color=app.NEGATIVE_COLOR,
                background_normal=app.NEUTRAL
                )

            layout2.add_widget(deleteButton)
            layout2.add_widget(cancelButton)
            layout.add_widget(layout2)

            popup =Popup(title='Delete Preset',
                content=layout,
                size_hint=[0.5, 0.7],
                auto_dismiss=False)

            popup.open()

            deleteButton.bind(
                on_press=lambda x:ScrOne().delete_vars(),
                on_release=popup.dismiss
                )
            cancelButton.bind(on_press=popup.dismiss)

        else:
            print 'no'
            ScrOne().warning_pop()
            
    def delete_vars(self):
            app = App.get_running_app()
            sel = app.USER_SELECTION
            print 'selection {}'.format(sel)
            parent = session.query(UserPreferences).filter_by(user_batch_name=sel)

            for items in parent:
                print '*-- parent --*\nid: {}\nuser_batch_name: {}\desired_m: {}\ntype_of_cacao: {}'.format(items.id, items.user_batch_name, items.desired_moisture, items.type_of_cacao)
                # child = items.same_batch

                session.delete(items)
                session.commit()

                # print '*-- child --*\nid: {}\nbatch_datetime: {}\nbatch_type_of_cacao: {}\nbatch_desired_moisture_content: {}'.format(child.id, child.batch_datetime, child.batch_type_of_cacao, child.batch_desired_moisture_content)
            # session.delete(parent)
            # session.commit()

    def search_vars(self):
        app = App.get_running_app()
        user_input = self.ids.search_bar.text

        # db_items = session.query(UserPreferences).filter_by(user_batch_name=user_input)
        # db_items = session.query(UserPreferences)
        # filt = db_items.filter(UserPreferences.user_batch_name.like(user_input)).all()

        # layout = BoxLayout(orientation='vertical')
        # layout2 = BoxLayout(orientation='horizontal')

        # print filt

        # layout2.add_widget(RecycleView(viewclass='SelectableButton', data=[{'text':str(x)} for x in filt], cols=2))
        # layout.add_widget(layout2)

        # popup = Popup(title='Search Results',
        #     content=layout,
        #     size_hint=[1, 0.9],
        #     auto_dismiss=True)

        # popup.open()
        app.USER_SELECTION = user_input

        print type(user_input)


        ScrFive().fetch_vars(self.manager)

class ScrTwo(Screen):
    '''
    create new preset screen
    '''

    def reset_vals(self, inputs):
        inputs.batch_name.text = ''
        inputs.input_desired_moisture.value = 7.0
        inputs.input_cacao_type.text = 'Select Cacao Type'

    def change_screen(self, instance):
        instance.current = 'home'

    def saveSet(self):
        '''
        save new user preference statement
        '''

        app = App.get_running_app()
        set1 = UserPreferences(
            type_of_cacao=app.USER_BATCH_TYPE,
            desired_moisture=app.USER_BATCH_MOISTURE,
            user_batch_name=app.USER_BATCH_NAME
            )
        # set1 = UserPreferences(user_batch_name=app.USER_BATCH_NAME, same_batch=set2)
        
        # session.add(set2)
        session.add(set1)
        session.commit()

        print ('name:{}\nmoisture:{}\ntype:{}').format(app.USER_BATCH_NAME, app.USER_BATCH_MOISTURE, app.USER_BATCH_TYPE)

    def warning_pop(self, text):
        layout = BoxLayout(orientation='vertical')
        layout.add_widget(Label(text='Please input a valid {}'.format(text)))
        closeButton = Button(text='Close')

        layout.add_widget(closeButton)

        popup = Popup(title='Warning!',
            content=layout,
            size_hint= [0.5, 0.7])

        popup.open()
        closeButton.bind(on_press=popup.dismiss)

    def save_pop(self, instance, inputs):
        app = App.get_running_app()

        def __init__(self, **kwargs):
            super(save_pop, self).__init__(**kwargs)

        layout=BoxLayout(orientation='vertical')
        layout2=BoxLayout(orientation='horizontal')
        layout.add_widget(Label(text='Are you sure you want to save?'))

        savebutton = Button(text='Yes, save it!',
            background_color=app.POSITIVE_COLOR,
            background_normal=app.NEUTRAL
            )

        closeButton = Button(
            text='No, let me edit',
            background_color=app.NEGATIVE_COLOR,
            background_normal=app.NEUTRAL
            )

        layout2.add_widget(savebutton)
        layout2.add_widget(closeButton)
        layout.add_widget(layout2)

        popup = Popup(title='Save Preset',
            content=layout,
            size_hint=[0.5, 0.7],
            auto_dismiss=False)

        popup.open()
        savebutton.bind(
            on_release=popup.dismiss,
            on_press=lambda x:(
                ScrTwo().saveSet(),
                ScrTwo().change_screen(instance),
                ScrTwo().reset_vals(inputs)
            )
        )
        closeButton.bind(
            on_press=popup.dismiss,
        )

    def exist_pop(self):
        layout = BoxLayout(orientation='vertical')
        layout.add_widget(Label(text='This name already exists'))
        closeButton = Button(text='Close')

        layout.add_widget(closeButton)

        popup = Popup(title='Warning!',
            content=layout,
            size_hint= [0.5, 0.7])

        popup.open()
        closeButton.bind(on_press=popup.dismiss)
    def get_vals(self):
        '''
        gets values from the input fields
        checks whether values are correct
        '''
        app = App.get_running_app()
        name = self.ids.batch_name.text
        desired_moisture = self.ids.input_desired_moisture_label.text
        cacao_type = self.ids.input_cacao_type.text

        if name and desired_moisture:
            if cacao_type != 'Select Cacao Type':
                check = session.query(exists().where(UserPreferences.user_batch_name.ilike(name))).scalar()
                if check == False:
                    app.USER_BATCH_NAME = name
                    app.USER_BATCH_MOISTURE = float(desired_moisture)
                    app.USER_BATCH_TYPE = cacao_type
                    
                    ScrTwo().save_pop(self.manager, self.ids)
                else:
                    ScrTwo().exist_pop()
            else:
                text = 'Cacao Type'
                ScrTwo().warning_pop(text)
        else:
            text = 'Batch Name'
            ScrTwo().warning_pop(text)

class ScrThree(Screen):
    '''
    cacao loading
    '''

    vals = ListProperty()
    batch_name = StringProperty()
    des_moist = StringProperty()
    c_type = StringProperty()

    def change_text(self, value):
        self.batch_name = str(value)

    def load_details(self):
        app = App.get_running_app()
        set1 = session.query(UserPreferences).filter_by(user_batch_name = app.USER_SELECTION).first()

        self.batch_name = str(set1.user_batch_name)
        self.des_moist = str(set1.desired_moisture)
        self.c_type = str(set1.type_of_cacao)

        self.canvas.ask_update()

    def quer(self):
        sel = session.query(UserPreferences)
        sel.all()

        for item in sel:
            print 'vals == {} {} {} {}'.format(item.id, item.user_batch_name, item.desired_moisture, item.type_of_cacao)

class ScrFour(Screen):

    def initiateBatch(self):
        '''
        Instantiate a new Batch for the selected UserPreference
        '''

        global checkid

        app = App.get_running_app()

        print app.USER_SELECTION

        cacaot = ''
        dm = 0.0
        name = ''
        queryName = session.query(UserPreferences).filter_by(user_batch_name=app.USER_SELECTION)
        for data in queryName:
            cacaot = data.type_of_cacao
            dm = data.desired_moisture
            name = data.user_batch_name

        # print cacaot
        # print dm
        # print name

        applyUserPref = BatchData(
            batch_type_of_cacao=cacaot,
            batch_desired_moisture_content=dm,
            batch_name=name)
        session.add(applyUserPref)
        session.commit()

        '''
        Saves current Batch ID for reference and assignment of foreign key
        '''
        checkid = applyUserPref.id

        print checkid

    '''
    For Simulation Purposes
    '''    
    def createD(self,sec):

        global checkid

        check = datetime.date.today()

        y = randint(30,60)
        z = randint(11,20)
        timet = datetime.datetime.now().time()

        print checkid

        setsave = EnvVars(batch_info=checkid,temperature=y,dry_weight=z,read_time=timet)
        session.add(setsave)
        session.commit()

    def etime(self, sec):
        '''
        Setting Elapsed Time
        '''

        global timer

        timer += 1

        set_elapsedt = str(datetime.timedelta(seconds=timer))

        self.elapsedt.text = set_elapsedt

    def plot(self,sec):

        global ax, ax2, xarr, yarr, zarr, checkid, counter, wetw, dryw, moistContent

        '''
        Checks for new data
        '''
        dataCheck = session.query(EnvVars).filter_by(batch_info=checkid,graphed=False).first()

        if not dataCheck:
            print 'No Data'
        else:
            timet = datetime.datetime.now().time()
            timet = timet.strftime("%I:%M %p")

            self.asof.text = timet

            container = []
            container2 = []
            yd = 0
            zd = 0

            check = datetime.date.today()

            getd = session.query(EnvVars).filter_by(batch_info=checkid,graphed=False)

            for data in getd:
                yd = data.temperature
                zd = data.dry_weight

            '''
            Computes Moisture Content
            ''' 
            if counter == 0:
                wetw = zd
                counter = counter + 1
            elif counter == 1:
                dryw = zd
                moistContent = ((wetw-dryw)/wetw) * 100
                wetw = dryw
                dryw = 0
                counter = counter + 1
            else:
                dryw = zd
                moistContent = ((wetw-dryw)/wetw) * 100
                wetw = dryw
                dryw = 0
                counter = 1

            gett = session.query(EnvVars.read_time).filter_by(batch_info=checkid,graphed=False)

            for data in gett:
                container = data
            for item in container:
                container2 = [item.strftime('%M:%S')]

            # print container2
            # print yd
            # print zd
            
            xarr.extend(container2)
            yarr += [yd]
            zarr += [zd]

            ax.cla()
            ax2.cla()
            plt.xticks(rotation=70)
            ax.plot(xarr,yarr, '-o')
            ax2.plot(xarr,zarr, '-ro')

            ax.set_title('Temp')
            ax2.set_title('Weight')

            canvas.draw()

            self.temp.text = str(yd)
            self.moist.text = str(moistContent)

            data = {'graphed': True}
            getd.update(data)
            session.commit()

    def update(self):
        self.eventUpdate = Clock.schedule_interval(self.plot,3)

    def savedata(self):
        self.eventSaveD = Clock.schedule_interval(self.createD,3)

    def updateE(self):
        self.eventUpdateE = Clock.schedule_interval(self.etime,1)

    def start(self):
        global fig

        self.initiateBatch()

        parent = BoxLayout(orientation = 'horizontal')
        graph = BoxLayout(orientation = 'vertical', padding = [0,10,0,0], size_hint = (2,1))
        stats = BoxLayout(orientation = 'vertical')
        stata = BoxLayout(orientation = 'vertical')

        graph.add_widget(canvas)

        stats.add_widget(Label(text = 'As of:', color=[0, 0, 0, 1]))
        stats.add_widget(Label(text = 'Temp:', color=[0, 0, 0, 1]))
        stats.add_widget(Label(text = 'Moisture:', color=[0, 0, 0, 1]))
        stats.add_widget(Label(text = 'Time Elapsed:', color=[0, 0, 0, 1]))

        self.asof = Label(text = '00:00', color=[0, 0, 0, 1])
        stata.add_widget(self.asof)
        self.temp = Label(text = 'C', color=[0, 0, 0, 1])
        stata.add_widget(self.temp)
        self.moist = Label(text = '0.00', color=[0, 0, 0, 1])
        stata.add_widget(self.moist)
        self.elapsedt = Label(text = '00:00', color=[0, 0, 0, 1])
        stata.add_widget(self.elapsedt)

        parent.add_widget(graph)
        parent.add_widget(stats)
        parent.add_widget(stata)
        self.add_widget(parent)
        fig.tight_layout()
        '''
        Uncomment if to Simulate
        '''
        # self.savedata()

        self.update()
        self.updateE()

# class ScrFive(Screen):
#     pass

class PresetSpinner(Spinner):
    vals = ListProperty()
    
    def get_vals(self):
        '''
        populates the search preset spinner with values
        from the db
        '''
        print '\n\n*-- function triggered --*\n\n'

        set1 = session.query(UserPreferences)

        self.canvas.ask_update()

        qr_len = set1.count()
        val_len = len(self.vals)
        
        if qr_len > val_len:
            '''
            triggers when something has been added to the DB
            '''
            for items in set1:
                if str(items.user_batch_name) not in self.vals:
                    self.vals.append(str(items.user_batch_name))
                    print '\n\none\n\n'
        elif qr_len < val_len:
            '''
            empties the whole ListProperty
            '''
            for items in self.vals:
                self.vals.remove(items)
                print '\n\ntwo point zero\n\n'

            '''
            then re-appends the existing values from DB
            '''
            for items in set1:
                    self.vals.append(str(items.user_batch_name))
                    print '\n\ntwo\n\n'

            if not qr_len:
                print '\n\n*-- no values --*\n\n'
                self.text = 'Press to search for saved presets...'
        else:
            '''
            triggers when qr_len is equal to val_len
            must do nothing
            '''
            print '\n\nthree\n\n'

        print '\n\n*-- values --*'
        print qr_len
        print val_len


def db_query(self):
    '''
    populates a list with db items
    '''
    print '\n\n*-- independent function triggered --*\n\n'

    set1 = session.query(UserPreferences)

    qr_len = set1.count()
    val_len = len(self.vals)
    
    if qr_len > val_len:
        '''
        triggers when something has been added to the DB
        '''
        for items in set1:
            if str(items.user_batch_name) not in self.vals:
                self.vals.append(str(items.user_batch_name))
                print '\n\none\n\n'
    elif qr_len < val_len:
        '''
        empties the whole ListProperty
        '''
        for items in vals:
            self.vals.remove(items)

        '''
        then re-appends the existing values from DB
        '''
        for items in set1:
                self.vals.append(str(items.user_batch_name))

        if not qr_len:
            self.text = 'Press to search for saved presets...'
    else:
        '''
        triggers when qr_len is equal to val_len
        must do nothing
        '''
        print '\n\nthree\n\n'

    print '\n\n*-- values --*'
    print qr_len
    print val_len

class ApplyButton(Button):
    
    def get_vals():
        pass

class SelectableRecycleGridLayout(FocusBehavior, LayoutSelectionBehavior, RecycleGridLayout):
    pass


class SelectableButton(RecycleDataViewBehavior, Button):
    ''' Add selection support to the Button '''
    index = None
    selected = BooleanProperty(False)
    selectable = BooleanProperty(True)

    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        self.index = index
        return super(SelectableButton, self).refresh_view_attrs(rv, index, data)

    def on_touch_down(self, touch):
        ''' Add selection on touch down '''
        if super(SelectableButton, self).on_touch_down(touch):
            return True
        if self.collide_point(*touch.pos) and self.selectable:
            return self.parent.select_with_touch(self.index, touch)

    def apply_selection(self, rv, index, is_selected):
        ''' Respond to the selection of items in the view. '''
        self.selected = is_selected

    def on_press(self):
        popup = TextInputPopup(self)
        popup.open()

    def update_changes(self, txt):
        self.text = txt

class SearchBar(TextInput, ActionItem):
    vals = ListProperty()
    sel = StringProperty()
    word_list = ('').split(' ')

    def __init__(self, *args, **kwargs):
        super(SearchBar, self).__init__(*args, **kwargs)
        self.hint_text='Tap to search'
    def search(self):
        request = self.text
        # self.suggestion_text = self.text
        # results = session.query(UserPreferences).filter_by(user_batch_name=self.text)
        # db_query(self)
        # self.sel = self.vals[0]
        if self.suggestion_text and keycode[1] == 'tab':
            self.insert_text(self.suggestion_text + ' ')
            return True

        return super(SearchBar, self)

    def on_text(self, instance, value):
        """ Include all current text from textinput into the word list to
        emulate the same kind of behavior as sublime text has.
        """
        self.suggestion_text = ''
        self.word_list = list(set(
            self.word_list + value[:value.rfind(' ')].split(' ')))
        val = value[value.rfind(' ') + 1:]
        if not val:
            return
        try:
            # grossly inefficient just for demo purposes
            word = [word for word in self.word_list
                    if word.startswith(val)][0][len(val):]
            if not word:
                return
            self.suggestion_text = word
        except IndexError:
            print 'Index Error.'

    def keyboard_on_key_down(self, window, keycode, text, modifiers):
        """ Add support for tab as an 'autocomplete' using the suggestion text.
        """
        if self.suggestion_text and keycode[1] == 'tab':
            self.insert_text(self.suggestion_text + ' ')
            return True
        return super(SearchBar, self).keyboard_on_key_down(window, keycode, text, modifiers)

class LabelA(Label):
    pass

class LabelB(Label):
    pass

class LabelC(Label):
    pass

class LabelD(Label):
    pass

class LabelE(Label):
    pass

class ShapePad(Label):
    pass

class SelectionPopup(Popup):
    obj = ObjectProperty(None)
    obj_text = StringProperty("")

    def __init__(self, obj, **kwargs):
        super(SelectionPopup, self).__init__(**kwargs)
        self.obj = obj
        self.obj_text = obj.text

class Container(RecycleDataViewBehavior, Button):
    ''' Add selection support to the Button '''
    index = None
    selected = BooleanProperty(False)
    selectable = BooleanProperty(True)

    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        self.index = index
        return super(Container, self).refresh_view_attrs(rv, index, data)

    def on_touch_down(self, touch):
        ''' Add selection on touch down '''
        if super(Container, self).on_touch_down(touch):
            return True
        if self.collide_point(*touch.pos) and self.selectable:
            return self.parent.select_with_touch(self.index, touch)

    def apply_selection(self, rv, index, is_selected):
        ''' Respond to the selection of items in the view. '''
        self.selected = is_selected

    def on_press(self):
        popup = SelectionPopup(self)
        popup.open()
        pass

    def update_changes(self, txt):
        self.text = txt

class ScrFive(Screen):
    # data_items = ([])
    data_items = ListProperty([])
    # data_items = StringProperty()

    # def __init__(self, **kwargs):
    #     super(ScrFive, self).__init__(**kwargs)
    #     # self.db = session.query(UserPreferences)
        # self.fetch_vars(self)
        # self.fetch_db()
        # self.db_items = session.query(UserPreferences).filter(UserPreferences.user_batch_name.like('test 1'))
    # Clock.schedule_once(lambda x: ScrFive().fetch_vars())

    # def fetch_db(self):
    #     db_items = session.query(UserPreferences)
    #     for row in db_items:
    #         self.data_items.append(str(row.user_batch_name))

    def fetch_vars(self, instance):
        app = App.get_running_app()
        
        db_items = session.query(UserPreferences)
        
        self.canvas.ask_update()

        qr_len = db_items.count()
        data_len = len(self.data_items)

        # if qr_len > data_len:
        #     for items in db_items:
        #         if str(items.user_batch_name) not in self.data_items:
        #             self.data_items.append(str(items.user_batch_name))
        # elif qr_len < data_len:
        #     for items in self.data_items:
        #         self.data_items.remove(items)
        #     for items in db_items:
        #         self.data_items.append(str(items.user_batch_name))
        # else:
        #     pass

        for items in db_items:
            self.data_items.append(
                '{}   -   {}   -   {}   -   {}   -   {}'.format(items.id, items.user_batch_name, items.type_of_cacao, items.desired_moisture, items.date_created.date())
            )

        # for row in db_items:
        #     if str(app.USER_SELECTION) == str(row.user_batch_name):
        #         # if str(row.user_batch_name) not in self.data_items:
        #         print row
        #         print row.user_batch_name
        #         self.data_items.append(str(row.user_batch_name))
        #         print self.data_items
        #     else:
        #         print 'not equal'

        instance.canvas.ask_update()    
        instance.current = 'results_page'    
        instance.canvas.ask_update()

class DrierApp(App):
    USER_BATCH_NAME = StringProperty('DUMMY1')
    USER_BATCH_TYPE = StringProperty('DUMMY2')
    USER_BATCH_MOISTURE = 0.0

    USER_SELECTION = StringProperty('')

    POSITIVE_COLOR = [228/255.0, 129/255.0, 68/255.0, 1]
    NEGATIVE_COLOR = [69/255.0, 44/255.0, 33/255.0, 1]
    NEUTRAL = 'img/white.png'

    icon = 'img/small_logo.png'
    title = 'Automated Cacao Dryer'

    # word_list = ('The quick brown fox jumps over the lazy old dog').split(' ')

    # def on_text(self, instance, value):
    #     """ Include all current text from textinput into the word list to
    #     emulate the same kind of behavior as sublime text has.
    #     """
    #     self.root.suggestion_text = ''
    #     word_list = list(set(
    #         self.word_list + value[:value.rfind(' ')].split(' ')))
    #     val = value[value.rfind(' ') + 1:]
    #     if not val:
    #         return
    #     try:
    #         # grossly inefficient just for demo purposes
    #         word = [word for word in word_list
    #                 if word.startswith(val)][0][len(val):]
    #         if not word:
    #             return
    #         self.root.suggestion_text = word
    #     except IndexError:
    #         print 'Index Error.'

    def build(self):
        # sm = ScreenManager(transition=FadeTransition())

        # screen1 = ScrOne(name='home')
        # screen2 = ScrTwo(name='new_preset')
        # sm.add_widget(screen1)
        # sm.add_widget(screen2)
        # db = MySQLdb.connect("localhost","testuser","test123","TESTDB")
        return Display()

if __name__ == '__main__':
    if not os.path.exists('db.sqlite3'):
        Base.metadata.create_all(engine)

    DrierApp().run()
    # WidgetsApp().run()
