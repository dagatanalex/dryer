# DC motor controller
import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

class DCMotor(object):
	'''
	class with methods for controlling the drier motor
	GPIO pin to be used must be connected to the relay switch
	do not forget constructors
	'''

	def __init__(self, pin):
		GPIO.setup(pin, GPIO.OUT)

	def sleeper(self):
		time.sleep(1)

	@staticmethod
	def turnOn(self, pin):
		self.sleeper()
		GPIO.output(pin, GPIO.HIGH)

		return "On"

	@staticmethod
	def turnOff(self, pin):
		self.sleeper()
		GPIO.output(pin, GPIO.LOW)

		return "Off"

	@staticmethod
	def cleaner(self):
		'''
		resets pin numbering
		call this method to reset pins back to normal
		pin may or may not be turned off before calling this method
		'''
		self.sleeper()
		GPIO.cleanup()

# To control the motor:
# 	cls = DCMotor(<pin number>)
# 	cls.turnOn(cls, <pin_number>)
# 	cls.turnOff(cls, <pin_number>)
# Do not forget to use try-catch-finally and run cleaner